class SceneMenu extends Phaser.Scene {
  constructor() {
    super({ key: 'SceneMenu' })
  }

  create() {
    this.title = this.add.text(
      0, 0,
      [
        'YOU HAVE ANGERED THE PEOPLE',
        'THE DILAWANS ARE ATTACKING MALACAÑANG',
        'PROTECT IT #OBOSEN'
      ],
      {
        fontFamily: 'VCR OSD Mono',
        fontSize: 18,
        color: '#ffffff',
        align: 'center',
        lineSpacing: 4
      }
    )

    this.play = this.add.text(
      0, 0, 
      'PLAY PEOPLE POWER 3',
      {
        fontFamily: 'VCR OSD Mono',
        fontSize: 18,
        color: '#000000',
        align: 'center',
        padding: 4,
        backgroundColor: '#ffffff'
      }
    ).setInteractive()

    this.play.on('pointerdown', function(e) {
      this.scene.scene.start('SceneMain')
    })
  
    this.title.x = this.game.config.width * 0.5
    this.title.y = (this.game.config.height - this.play.height) * 0.5
    this.title.setOrigin(0.5)
    this.title.setWordWrapWidth(this.game.config.width, true)

 
    this.play.x = this.game.config.width * 0.5
    this.play.y = this.title.y + this.title.height + 10
    this.play.setOrigin(0.5)
  }
}