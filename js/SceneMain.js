class SceneMain extends Phaser.Scene {
  constructor() {
    super({ key: 'SceneMain' })
  }

  preload() {
    this.load.image('palace', 'sprites/palace.png')
    this.load.image('mask1', 'sprites/mask.png')
    this.load.image('mask2', 'sprites/mask2.png')
    this.load.image('mask3', 'sprites/mask3.png')
    this.load.image('mask4', 'sprites/mask4.png')
    this.load.image('mask5', 'sprites/mask5.png')
    this.load.image('mask6', 'sprites/mask6.png')
    this.load.image('mask7', 'sprites/mask7.png')
    this.load.image('mask8', 'sprites/mask8.png')
    this.load.image('mask9', 'sprites/mask9.png')
    this.load.image('mask10', 'sprites/mask10.png')
    this.load.image('mask11', 'sprites/mask11.png')
    this.load.image('rock', 'sprites/rock.png')
    this.load.image('rock2', 'sprites/rock2.png')
    this.load.spritesheet('coin', 'sprites/coin.png', {
      frameWidth: 22,
      frameHeight: 22
    })
  }
  
  create() {
    this.anims.create({
      key: 'coin',
      frames: this.anims.generateFrameNumbers('coin'),
      frameRate: 20,
      repeat: -1
    })

    this.dilawans = this.add.group()
    this.dilawanRocks = this.add.group()
    this.playerRocks = this.add.group()
    this.coins = this.add.group()
    
    this.palace = new Palace(
      this,
      this.game.config.width * 0.5,
      this.game.config.height * 0.5, 
      'palace'
    )

    this.time.addEvent({
      delay: 1000,
      callback: () => {
        if (Phaser.Math.Between(1, 3) === 1) {
          this.dilawans.add(new Dilawan(
            this,
            Phaser.Math.Between(0, this.game.config.width),
            Phaser.Math.Between(0, this.game.config.height),
            'mask' + Phaser.Math.Between(1, 11)
          ))
        }
        if (Phaser.Math.Between(1, 5) === 1) {
          this.coins.add(new Coin(
            this,
            Phaser.Math.Between(0, this.game.config.width),
            Phaser.Math.Between(0, this.game.config.height)
          ))
        }
      },
      callbackScope: this,
      loop: true
    })

    this.input.on('pointerdown', function() {
      let rock = new PlayerRock(
        this.scene,
        this.x,
        this.y
      );
      this.scene.playerRocks.add(rock);
   })

    this.physics.add.overlap(this.palace, this.dilawanRocks, function(palace, rock) {
      rock.destroy()
      palace.hit()
    })

    this.physics.add.overlap(this.dilawans, this.playerRocks, function(dilawan, rock) {
      rock.destroy()
      dilawan.kill()
    })
  }

  update() {
    this.dilawanRocks.getChildren().forEach((rock) => {
      if (rock.x < -rock.displayWidth ||
        rock.x > this.game.config.width + rock.displayWidth ||
        rock.y < -rock.displayHeight ||
        rock.y > this.game.config.height + rock.displayHeight) {
        if (rock) {
          rock.destroy()
        }
      }
    })
  }
}