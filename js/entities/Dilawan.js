class Dilawan extends Entity {
  constructor(scene, x, y, key) {
    super(scene, x, y, key)
    this.body.velocity.x = Phaser.Math.Between(-100, 100)
    this.body.velocity.y = Phaser.Math.Between(-100, 100)
    this.body.setBounce(1, 1)
    this.body.setCollideWorldBounds(true)

    this.shootTimer = this.scene.time.addEvent({
      delay: 1000,
      callback: this.shoot,
      callbackScope: this,
      loop: true
    })
  }

  shoot() {
    let rock = new Rock(
      this.scene,
      this.x,
      this.y
    )
    this.scene.dilawanRocks.add(rock);
  } 

  kill() {
    if (this.shootTimer !== undefined) {
      if (this.shootTimer) {
        this.shootTimer.remove(false)
        this.destroy()
      }
    }
  }
}
