class Palace extends Entity {
  constructor(scene, x, y, key) {
    super(scene, x, y, key)
    this.fullHealth = 1000 
    this.setData('health', this.fullHealth)
    this.setData('money', 10)
    this.healthBar = new HealthBar(scene, this.x - this.width/2, 40, this.width)
    this.moneyStat = new MoneyStat(scene, 20, 20, this.getData('money'))
  }

  isDestroyed() {
    return this.getData('health') <= 0
  }

  collectMoney(value) {
    const currentMoney = this.getData('money')
    const newMoney = currentMoney + value
    this.setData('money', newMoney)
    this.moneyStat.update(newMoney)
  }

  destroy() {
    this.scene.time.addEvent({
      delay: 1000,
      callback: function() {
        this.scene.scene.start('SceneGameOver')
      },
      callbackScope: this,
      loop: false
    })
  }

  hit() {
    this.healthBar.update(this.getData('health') / this.fullHealth)
    if (this.isDestroyed()) {
      this.destroy()
    } else {
      const currentHealth = this.getData('health')
      this.setData('health', currentHealth - 1)
    }
  }
}
