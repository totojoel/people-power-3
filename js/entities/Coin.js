class Coin extends Entity {
  constructor(scene, x, y) {
    super(scene, x, y, 'coin')
    this.play('coin')
    this.setInteractive()
    this.on('pointerdown', () => {
      scene.palace.collectMoney(10)
      this.expire()
    })

    this.expireTime = this.scene.time.addEvent({
      delay: 3000,
      callback: this.expire,
      callbackScope: this,
      loop: true
    })
  }

  expire() {
    if (this.expireTime !== undefined) {
      if (this.expireTime) {
        this.expireTime.remove(false)
        this.destroy()
      }
    }
  }
}
