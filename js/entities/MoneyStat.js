class MoneyStat {
  constructor (scene, x, y, value) {
    this.x = x
    this.y = y
    this.value = value
    this.scene = scene
    this.draw()
  }

  update(value) {
    console.log('collecting')
    this.value = value
    this.draw()
  }

  draw() {
    
    this.text = this.scene.add.text(
      0, 0,
      this.value,
      {
        fontFamily: 'VCR OSD Mono',
        fontSize: 16,
        color: '#ffffff',
        align: 'center',
        backgroundColor: '#000000'
      }
    )
    this.text.x = this.x
    this.text.y = this.y
  }
}
