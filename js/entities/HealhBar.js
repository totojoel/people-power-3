class HealthBar {
  constructor (scene, x, y, length) {
    this.bar = new Phaser.GameObjects.Graphics(scene)
    this.x = x
    this.y = y
    this.length = length
    this.percent = 1
    this.draw()
    scene.add.existing(this.bar)
  }

  update(percent) {
    this.percent = percent
    this.draw()
  }

  draw() {
    this.bar.clear()
    this.bar.fillStyle(0xffc425)
    this.bar.fillRect(this.x, this.y, this.length, 4)
    this.bar.fillStyle(0xd11141)
    this.bar.fillRect(this.x, this.y, this.length * this.percent, 4)
  }
}
