class Rock extends Entity {
  constructor(scene, x, y) {
    super(scene, x, y, 'rock')
    if (x > this.scene.palace.x ) {
      this.body.velocity.x = -300
    }
    if (x < this.scene.palace.x ) {
      this.body.velocity.x = 300
    }
    if (y > this.scene.palace.y ) {
      this.body.velocity.y = -300
    }
    if (y < this.scene.palace.y ) {
      this.body.velocity.y = 300
    }
  }
}
