const config = {
  type: Phaser.WEBGL,
  width: window.innerWidth,
  height: window.innerHeight,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { x: 0, y: 0 },
      // debug: true
    }
  },
  pixelArt: true,
  roundPixels: true,
  backgroundColor: 'black',
  scene: [
    // SceneMenu,
    SceneMain,
    // SceneGameOver,
  ]
}

const game = new Phaser.Game(config)