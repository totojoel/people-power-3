class SceneGameOver extends Phaser.Scene {
  constructor() {
    super({ key: 'SceneGameOver' })
  }

  create() {
    this.title = this.add.text(
      0, 0,
      [
        'GAME OVER',
        'THE DILAWANS HAS TAKEN OVER MALACAÑANG',
      ],
      {
        fontFamily: 'VCR OSD Mono',
        fontSize: 18,
        color: '#ffffff',
        align: 'center',
        lineSpacing: 4
      }
    )

    this.playAgain = this.add.text(
      0, 0, 
      'PLAY AGAIN',
      {
        fontFamily: 'VCR OSD Mono',
        fontSize: 18,
        color: '#000000',
        align: 'center',
        padding: 4,
        backgroundColor: '#ffffff'
      }
    ).setInteractive()
  
    this.menu = this.add.text(
      0, 0, 
      'MAIN MENU',
      {
        fontFamily: 'VCR OSD Mono',
        fontSize: 18,
        color: '#000000',
        align: 'center',
        padding: 4,
        backgroundColor: '#ffffff'
      }
    ).setInteractive()

    this.playAgain.on('pointerdown', function(e) {
      this.scene.scene.start('SceneMain')
    })
    this.menu.on('pointerdown', function(e) {
      this.scene.scene.start('SceneMenu')
    })

    this.title.x = this.game.config.width * 0.5
    this.title.y = (this.game.config.height - this.playAgain.height) * 0.5
    this.title.setOrigin(0.5)
    this.title.setWordWrapWidth(this.game.config.width, true)

 
    this.playAgain.x = this.game.config.width * 0.5 - this.menu.width * 0.5 - 10
    this.playAgain.y = this.title.y + this.title.height + 10
    this.playAgain.setOrigin(0.5)

    this.menu.x = this.game.config.width * 0.5 + this.playAgain.width * 0.5 + 10
    this.menu.y = this.playAgain.y
    this.menu.setOrigin(0.5)
  }
}